using GerenciadoDeUsuarios.Models;
using Microsoft.EntityFrameworkCore;

namespace GerenciadoDeUsuarios.DbContexts
{
    public class AppContext : DbContext
    {
        public DbSet<Usuario> Usuarios { get; set; }
        public DbSet<Permissao> Permissoes { get; set; }
        
        protected override void OnConfiguring(DbContextOptionsBuilder builder)
        {
            builder.UseSqlite("Data Source=App.db");
        }

    }
}