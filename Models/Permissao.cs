using System;

namespace GerenciadoDeUsuarios.Models
{
    public class Permissao
    {
        public Guid PermissaoId { get; set; }        
        public string Nome { get; set; }

    }
}