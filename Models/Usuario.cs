using System;
using System.Collections.Generic;

namespace GerenciadoDeUsuarios.Models
{
    public class Usuario
    {
        public Guid UsuarioId { get; set; }
        public string Nome { get; set; }
        public string Email { get; set; }
        public string Senha { get; set; }
        public IList<Permissao> Permissoes {get;set;}

    }
}